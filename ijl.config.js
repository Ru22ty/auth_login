const pkg = require('./package')

module.exports = {
    "apiPath": "stubs/api",
    "webpackConfig": {
        "output": {
            "publicPath": `/static/auth_login/${pkg.version}/`
        }
    },
}
